﻿//#define DEV
using UnityEngine;
using System.Collections;

using System.Collections.Generic;

/*
 * GameBoard.cs is the central command for this project
 * Implements a state machine to organize game flow
 * Contains all cell references, main Camera reference, GUI reference, and allows state machine to communicate with all of these
 */
public class GameManager : MonoBehaviour 
{

	#region vars
	[HideInInspector]
	public Cell[] cells;
	public Cell3D[] c3lls;
	public PetriDish settings; // Settings for building the petri dish / board
	public SettingsGUI settingsGUI;
	public GameObject cellPrefab;
	public GameCam cam;
	public GameObject floor;
	public AnimationCurve stepCurve;
	/*
	[HideInInspector]
	public bool phase = true; // Cells require two-phase process in order to concurrently analyze and move
	[HideInInspector]
	public int steps; // Number of steps that the current simulation has been running
	[HideInInspector]
	public int liveCells; // Number of cells currently alive
	[HideInInspector]
	public bool isThreeDim;
	*/
	[HideInInspector]
	public StateInfo stateInfo;

	protected FiniteStateMachine<GameManager> fsm = new FiniteStateMachine<GameManager>();

	#endregion

	#region Unity methods
	void Start () 
	{
		fsm.Initialize(this, new InitializeState());
		settingsGUI.manager = this;
	}

	void Update () 
	{
		fsm.Update();
	}
	#endregion


	#region Actions
	/// <summary>
	/// Seeks for Nearest Cell
	/// Since some cells may be too small for collision, RayCastHit.GetComponent isn't feasible
	/// Also, Cells do not have colliders in order to bump a tiny bit more performance out of the system, allowing for larger grids
	/// </summary>
	/// <returns>Closest Cell</returns>
	/// <param name="p">Mouse Location</param>
	public Cell SeekCell(Vector3 p)
	{
		Cell closest = null;
		float distance = 8; 
		for(int i = 0; i < cells.Length; i++)
		{
			float d = Vector3.Distance(p, cells[i].trans.position);

			if(d < distance)
			{
				closest = cells[i];
				distance = d;
			}
		}
		return closest;
	}
	

	public void ChangeState(GameBaseState state)
	{
		fsm.ChangeState(state);
	}
	
	/// <summary>
	/// Takes in button messages from GUI and passes it along to State Machine
	/// </summary>
	/// <param name="b">Button Enum</param>
	public void Button(BUTTONS b)
	{
		fsm.Button(b);
	}
	#endregion

}
