﻿using UnityEngine;
using System.Collections;

/*
 * PlayState.cs moves cells and initiates check on each cell to determine its future
 * This is the main state that drives cell behavior
 */
public class PlayState : GameBaseState 
{

	protected bool check = true; // Is it time to check on the cell's next life?
	protected bool playing = true; // Are we in play mode or step mode?
	protected bool phase = true; // Necessary to between two phases, current and next, so current movement and checking next step can be executed concurrently
	protected int liveCells;	

	public override void Enter (GameManager m)
	{
		base.Enter(m);
		phase = manager.stateInfo.phase; // a boolean value "phase" is used to determine whether we are in one of two phases. Necessary in order to simultaneously move and check neighbors
		timer = Time.fixedTime;

		manager.StartCoroutine(CheckNextStep());
	}

	public override void Execute ()
	{
		if(!playing)
		{
			if(Time.fixedTime - timer < 1)
				StepMove();
		}
		else
			ContinuousMove(); 
	}

	public override void Exit ()
	{
		//manager.stateData.phase = phase; // Store phase for pause
		manager.StopCoroutine("CheckNextStep");
	}

	public override void Button (BUTTONS b)
	{
		switch (b)
		{
			case BUTTONS.REVEALSETTINGS:
				manager.ChangeState(new RevealingSettings());
				break;
			case BUTTONS.EDITCELLS:
				manager.ChangeState(new PreEditCellsState());
				break;
			case BUTTONS.PLAY:
				playing = true;
				break;
			case BUTTONS.PAUSE:
				playing = false;
				break;
			case BUTTONS.NEXT:
				if(Time.fixedTime - timer >= 1) // Do not advance step if we haven't finished the current step
				{
					timer = Time.fixedTime;
					manager.StartCoroutine(CheckNextStep());
				}
				break;
			case BUTTONS.RESETGAME:
				Reset();
				break;
		}
	}

	protected virtual void Reset()
	{
		manager.stateInfo.reset = true;
		manager.ChangeState(new InitializeState());
	}

	/// <summary>
	/// Calls on cells to execute their growth/death behavior
	/// </summary>
	protected virtual void ContinuousMove()
	{
		if(check)
			manager.StartCoroutine(CheckNextStep());

		for(int i = 0; i < manager.cells.Length; i++)
			manager.cells[i].Move(phase, manager.settings.cellSpeed);

	}

	protected virtual void StepMove()
	{
		for(int i = 0; i < manager.cells.Length; i++)
			manager.cells[i].Move(phase, manager.settings.cellSpeed);
	}


	
	/// <summary>
	/// Calls on each cell to make a check of its neighbors and to set its next life based on The Rules
	/// </summary>
	/// <returns></returns>
	protected virtual IEnumerator CheckNextStep()
	{
		AnalyzeCells();
		check = false; // Do not let the check execute again until we are finished with the time frame
		
		yield return new WaitForSeconds(1f/ (float)manager.settings.gameSpeed ); // Determines how "fast" the simulation runs
		phase = !phase; // Swap phase
		check = manager.stateInfo.liveCells > 0;
		manager.stateInfo.steps++;
	}

	protected virtual void AnalyzeCells()
	{
		liveCells = 0;
		foreach(Cell cell in manager.cells)
		{
			if(cell.CheckNextStep(ref manager.cells, phase)) // Execute cell analysis
				liveCells++;
		}
		manager.stateInfo.liveCells = liveCells; 
	}
}
															