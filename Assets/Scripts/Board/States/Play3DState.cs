﻿using UnityEngine;
using System.Collections;

public class Play3DState : PlayState 
{
	
	protected override void Reset ()
	{
		manager.stateInfo.reset = true;
		manager.ChangeState(new Initialize3DState());
	}

	protected override IEnumerator CheckNextStep()
	{
		check = false; // Do not let the check execute again until we are finished with the time frame
		AnalyzeCells();
		yield return new WaitForSeconds(1f/ (float)manager.settings.gameSpeed ); // Determines how "fast" the simulation runs
		check = liveCells > 0; // If all cells are dead, don't bother to check on the petri dish
		phase = !phase; // Swap phase
		manager.stateInfo.steps++;
	}

	protected override void AnalyzeCells()
	{
		liveCells = 0;
		foreach (Cell3D cell in manager.c3lls)
			cell.CheckHorizontal(ref manager.c3lls, phase);
		
		foreach (Cell3D cell in manager.c3lls)
		{
			if (cell.CheckVertical(ref manager.c3lls, phase))
				liveCells++;
		}
		foreach (Cell cell in manager.c3lls)
			cell.planeCellcount = 0;

		manager.stateInfo.liveCells = liveCells;
	}

	protected override void ContinuousMove()
	{
		if (check)
			manager.StartCoroutine(CheckNextStep());

		for (int i = 0; i < manager.c3lls.Length; i++)
			manager.c3lls[i].Move(phase, manager.settings.cellSpeed);

	}

	protected override void StepMove()
	{
		for (int i = 0; i < manager.c3lls.Length; i++)
			manager.c3lls[i].Move(phase, manager.settings.cellSpeed);
	}

}
