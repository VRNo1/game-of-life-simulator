﻿using UnityEngine;
using System.Collections;
				   
/*
 * PreEditCellsState.cs handles moving the camera from "play" position to "edit the petri dish" position
 * That's all, really
 */
public class PreEditCellsState : GameBaseState 
{
	protected float transitionTime = 3f;

	public override void Enter (GameManager m)
	{
		manager = m;
		manager.cam.speed = 0;
		manager.settingsGUI.settings.isLifeAltering = true; // We are about to irrevocably alter the life of one petri dish
	}
	
	public override void Execute ()
	{

		if(manager.cam.MoveToEdit())
			manager.ChangeState(new EditCellsState());
	}
	
	public override void Exit ()
	{
		
	}

}
