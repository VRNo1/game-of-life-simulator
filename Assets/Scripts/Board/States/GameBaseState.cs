using UnityEngine;
using System.Collections;

// GameBaseState.cs is the One State to Rule Them All
// all gamestates derive from this, in order to provide common button access and GameManager-specific object access
public class GameBaseState : FSMState<GameManager>
{

	protected GameManager manager;
	protected float timer;
	protected float maxTime = 1f;

	/// <summary>
	/// Enter is executed once at the beginning of the state cycle
	/// It will execute AFTER the previous state runs it's Exit() function
	/// </summary>
	/// <param name="m"></param>
	public override void Enter (GameManager m)
	{

		manager = m; // A reference to the manager is required for much that we do
		if (manager.stateInfo == null)
			manager.stateInfo = new StateInfo();
	}
	
	/// <summary>
	/// Execute is called once per frame, so long as the GameManager is being updated by unity
	/// </summary>
	public override void Execute ()
	{

	}

	/// <summary>
	/// Exit is called once on entering a new state, BEFORE the next state calls its Enter() function
	/// </summary>
	public override void Exit ()
	{

	}

	/// <summary>
	/// Convenient button access from the GUI, via GameManager
	/// </summary>
	/// <param name="b"></param>
	public override void Button (BUTTONS b)
	{

	}
}
