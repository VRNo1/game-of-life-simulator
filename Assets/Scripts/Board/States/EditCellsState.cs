﻿using UnityEngine;
using System.Collections;

/*
 * EditCellsState.cs handles adding and removing cells from the petri dish
 */
public class EditCellsState : GameBaseState
{
	protected bool firstHit; // on the first touch of a cell, find out whether we're adding or subtracting cells. then enter "paint" mode in that state
	protected bool activate; // are we activating or deactivating cells?
	protected float camSpeed; // stop the camera from moving while we're in edit mode, and reset it afterwards

	public override void Enter (GameManager m)
	{
		manager = m;
	}

	public override void Execute ()
	{

		if(Input.GetMouseButtonDown(0) && !Input.GetKey(KeyCode.LeftShift))
			firstHit = true; 

		if(!Input.GetMouseButton(0) || Input.GetKey(KeyCode.LeftShift))
			return;

		if(Input.GetMouseButtonUp(0))
			firstHit = false;

		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;

		if(Physics.Raycast(ray, out hit))
		{
			Cell cell = manager.SeekCell(hit.point); // Find the closest cell based on coordinates. Cells have no colliders to use with Physics.Raycast. this is intentional
			if(cell)
			{
				if(firstHit)
				{
					activate = !cell.ReportLife(manager.stateInfo.phase);
					firstHit = false;
				}

				cell.SetState(manager.stateInfo.phase, activate); // Turn cell on or off, according to it's current phase
			}
		}
	}

	public override void Exit ()
	{
		manager.cam.speed = .2f;
	}

	public override void Button (BUTTONS b)
	{
		if(b == BUTTONS.RESUMEGAME)
			manager.ChangeState(new PostEditCellsState());
	}
}
