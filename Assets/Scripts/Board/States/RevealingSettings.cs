﻿using UnityEngine;
using System.Collections;

/*
 *  RevealingSettings.cs reveals the GUI pane for game settings
 *  handles movement of GUI pane and setting GUI to isVisible
 */
public class RevealingSettings : GameBaseState
{
	protected float revealTime = .7f;
	protected SettingsGUI gui;

	public override void Enter (GameManager m)
	{
		manager = m;
		gui = manager.settingsGUI;
		gui.settings.isVisible = true;
		timer = Time.time;
	}
	
	public override void Execute ()
	{
		float t = gui.curve.Evaluate((Time.time - timer) / revealTime);
		gui.Slide(t);
		if(t >= 1)
			manager.ChangeState(new EditSettingsState());
	}
	
	public override void Exit ()
	{

	}


}
