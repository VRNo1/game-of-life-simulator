﻿using UnityEngine;
using System.Collections;

/*
 * 	EditSettingsState.cs does nothing but hold space while the user changes GUI settings
 * 	Handles button input to exit this state
 */ 
public class EditSettingsState : GameBaseState
{

	protected SettingsGUI gui;


	public override void Enter (GameManager m)
	{
		manager = m;
		gui = manager.settingsGUI;
	}

	public override void Execute ()
	{
	
	}

	public override void Exit ()
	{

	}

	public override void Button (BUTTONS b)
	{
		if(b == BUTTONS.HIDESETTINGS)
				manager.ChangeState(new HidingSettings());
	}
}
