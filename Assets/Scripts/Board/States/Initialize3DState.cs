﻿using UnityEngine;
using System.Collections;

public class Initialize3DState : InitializeState 
{

	public override void Enter (GameManager m)
	{
		base.Enter (m);
		manager.stateInfo.isThreeDim = true;
		manager.floor.SetActive(false);
	}

	public override void Execute ()
	{
		//Debug.Log("initializing");
		if(finished)
			manager.ChangeState(new Play3DState());
	}

	protected override IEnumerator CreatePoints() 
	{
		// If resetting midgame, remove previous array of cells and destroy each cell
		if(manager.stateInfo.reset)
		{
			DestroyCells();
			manager.stateInfo.isThreeDim = true;
			manager.stateInfo.reset = false;
		}

		int index = 0;
		int w = manager.settings.gridWidth; // Equal dimensions only for 3D

		manager.c3lls = new Cell3D[(int)Mathf.Pow(w, 3)];

		// Set up cell array and assign settings
		for(int i = 0; i < w; i++)
		{
			for(int j = 0; j < w; j++)
			{
				for(int k = 0; k < w; k++)
				{
					Vector3 pos = new Vector3(i, j, k);
					GameObject obj = (GameObject)GameObject.Instantiate(manager.cellPrefab);
					
					//TODO: Factory to create cells rather than this mess

					GameObject.Destroy(obj.GetComponent<Cell>());
					obj.AddComponent<Cell3D>();
					Cell3D cell = obj.GetComponent<Cell3D>();
					manager.c3lls[index] = cell;
					cell.transform.parent = manager.transform;
					cell.Initialize(obj.transform, pos, manager.settings.boardScale, w, manager.settings.cellMaxSize);
					cell.index = index;
					
					cell.SetAdjacent(i, j);
					
					// Basic randomize. nothing fancy. 
					if(Random.Range (0, 100) < manager.settings.initialPercent)
						cell.SetState(true, true);
					
					index++;
				}
			}
			yield return null; // Reduce the load for larger grids, also creates an incremental populating effect
		}

		for(int i = 0; i < manager.c3lls.Length; i++)
		{
			manager.c3lls[i].GetVerticals(manager.c3lls.Length);
		}

		finished = true;
	}


}
