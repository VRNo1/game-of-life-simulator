﻿using UnityEngine;
using System.Collections;

public class Cell3D : Cell 
{

	protected int[] verticalAdjacent;

	public void Initialize(Transform t, Vector3 pos, float scale, int s, float size)
	{
		trans = t;
		rendr = renderer;
		rendr.material.SetColor("_Color", doomColor);
		trans.localPosition = new Vector3((pos.x - s/2)* scale, (pos.y - s/2)* scale, (pos.z - s/2) * scale); // Set position of cell so that the petri dish is built around world zero
		trans.localScale = Vector3.one * deathSize; // Initialize as small
		width = height = s;
		lifeSize = size;
		rendr.useLightProbes = true;
	}


	public override void CheckHorizontal(ref Cell3D[] cells, bool curPhase)
	{
		int liveCells = 0;
		for(int i = 0; i < adjacent.Length; i++)
		{
			if(cells[adjacent[i]].ReportLife(curPhase))
				liveCells++; 
		}
		planeCellcount = liveCells;
	}

	public override bool CheckVertical(ref Cell3D[] cells, bool curPhase)
	{
		int liveCells = planeCellcount;
		for(int i = 0; i < verticalAdjacent.Length; i++)
			liveCells += cells[verticalAdjacent[i]].planeCellcount;

		return SetLife(!curPhase, (curPhase ? lifePhaseOne : lifePhaseTwo) && (liveCells >= 5 && liveCells <= 7) ? true : (liveCells == 6 || liveCells == 7));
	}

	/// <summary>
	/// After 2D neighbors are set, get vertical neighbors
	/// </summary>
	public override void GetVerticals(int cellCount)
	{
		int[] adj = {-1, -1};
		int plane = width * width;

		if(index + plane < cellCount)
			adj[0] = index + plane;
		if(index - plane >= 0)
			adj[1] = index - plane;

		int c = adj[0] != -1 && adj[1] != -1 ? 2 : 1;
		verticalAdjacent = new int[c];

		if(adj[0] != -1 && adj[1] != -1)
		{
			verticalAdjacent[0] = adj[0];
			verticalAdjacent[1] = adj[1];
		}
		else
			verticalAdjacent[0] = adj[0] >= 0 ? adj[0] : adj[1];
	}
}
