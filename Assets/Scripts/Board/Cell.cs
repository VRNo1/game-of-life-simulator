﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Cell.cs: The Reason we're all here
/// Cell.cs handles its own life, checking neighbors, reacts to phase, shrinks and grows, changes color
/// </summary>
public class Cell : MonoBehaviour 
{
	#region vars
	public bool lifePhaseOne; // Two phases are necessary, so that next step check and current step movement can execute concurrently
	public bool lifePhaseTwo; 
	public int index; // The linear index of this cell in an array in gamemanager
	public int[] adjacent; // This cell's neighbors
	public int planeCellcount;
	public Color livingColor; // Color to turn when alive
	public Color doomColor; // What is your favorite color?
	public Transform trans;

	protected int width;
	protected int height;
	protected int lifeTime = 0;
	protected float lifeSize;
	protected float deathSize = .1f; // Size to shrink to on "death"
	protected Renderer rendr;

	#endregion

	#region initialize
	public void Initialize(Transform t, int x, int z, float scale, int w, int h, float size)
	{
		trans = t;
		rendr = renderer;
		//livingColor = rendr.material.GetColor("_Color");
		rendr.material.SetColor("_Color", doomColor);
		trans.localPosition = new Vector3((x - w/2)* scale, 0, (z - h/2) * scale); // Set position of cell so that the petri dish is built around world zero
		trans.localScale = Vector3.one * deathSize; // Initialize as small
		width = w;
		height = h;
		lifeSize = size;
		lifeTime = 0;
		rendr.useLightProbes = true;
	}
	#endregion

	#region playmode
	/// <summary>
	/// Checks all neighbors' current phase to determine whether this cell should live or die next turn
	/// </summary>
	/// <param name="nodes"></param>
	/// <param name="curPhase"></param>
	/// <returns></returns>
	public virtual bool CheckNextStep(ref Cell[] nodes, bool curPhase) 
	{
		int liveCells = 0;
		for(int i = 0; i < adjacent.Length; i++)
		{
			if(nodes[adjacent[i]].ReportLife(curPhase))
				liveCells++; 
		}

		// Set results to upcoming phase, rather than the currently executing phase
		// If current phase is alive AND there are two OR three live cells, set next phase to TRUE
		// Otherwise, send a check against 3 live cells to determine cell revival of this dead cell
		return SetLife(!curPhase, (curPhase ? lifePhaseOne : lifePhaseTwo) && (liveCells == 2 || liveCells == 3) ? true : liveCells == 3); 
	}
	
	/// <summary>
	/// Is this cell currently alive?
	/// </summary>
	/// <param name="phaseOne">current phase</param>
	/// <returns>dead or alive</returns>
	public bool ReportLife(bool phaseOne)
	{
		return phaseOne ? lifePhaseOne : lifePhaseTwo;
	}
	
	/// <summary>
	/// Set current dead/alive state immediately
	/// This is accessed during the petri dish editing state
	/// </summary>
	/// <param name="phase"></param>
	/// <param name="alive"></param>
	public void SetState(bool phase, bool alive)
	{
		if(phase)
			lifePhaseOne = alive;
		else
			lifePhaseTwo = alive;

		trans.localScale = Vector3.one * (alive ? lifeSize : deathSize);
		rendr.material.SetColor("_Color", (alive ? livingColor : doomColor));
	}
	
	/// <summary>
	/// Execute "movement"
	/// Cell grows on "life" and shrinks on "die"
	/// </summary>
	/// <param name="phaseOne">current phase</param>
	/// <param name="spd">how fast to grow or shrink</param>
	public void Move(bool phaseOne, float amt)
	{
		Vector3 targetScale = Vector3.one * (phaseOne ? (lifePhaseOne ? lifeSize : deathSize) : (lifePhaseTwo ? lifeSize : deathSize)); 
		trans.localScale = Vector3.Lerp(trans.localScale, targetScale, amt * .04f);
		bool living = phaseOne ? lifePhaseOne : lifePhaseTwo;
		rendr.material.SetColor("_Color", TargetColor(living, amt));
	
	}
	
	/// <summary>
	/// Return life or death color
	/// If a cell is alive for more than two turns in a row, it will begin to accumulate color, "blooming" with life, until it is totally white
	/// </summary>
	/// <param name="alive"></param>
	/// <returns></returns>
	protected Color TargetColor(bool alive, float amt)
	{
		Color c = rendr.material.GetColor("_Color");
		//return alive ? Color.Lerp(c, c + (livingColor * .1f), Time.fixedDeltaTime * amt) : doomColor;
		return Color.Lerp(c, (alive ? c + (livingColor * .1f) : doomColor) , Time.fixedDeltaTime * amt);
	}
	
	/// <summary>
	/// Set next phase of life in preparation for the next phase of movement
	/// </summary>
	/// <param name="altPhase">next phase</param>
	/// <param name="state">alive or dead?</param>
	/// <returns></returns>
	protected bool SetLife(bool altPhase, bool state)
	{
		if(altPhase)
			return lifePhaseOne = state;
		else
			return lifePhaseTwo = state;
	}
	#endregion


	#region initialize
	/// <summary>
	/// SetAdjacent is responsible for searching and determining neighbors, establishing references in an array for rapid access
	/// </summary>
	/// <param name="w">current horizontal grid index</param>
	/// <param name="h">current vertical grid index</param>
	public void SetAdjacent(int w, int h)
	{

		int adj = 0;
		int[] indices = new int[8]; // total possible neighbors

		// This is the long way of doing this. should be assigning two ways in one pass
		// There are far more elegant ways to accomplish this. If nothing else, this part right here is where my sleep deprivation is showing
		if(w > 0)
		{
			indices[adj] = index - width;
			adj++;
		}
		if(w < width - 1)
		{
			indices[adj] = index + width;
			adj++;
		}
		if(h > 0)
		{
			indices[adj] = index - 1; 
			adj++;
		}
		if(h < height - 1)
		{
			indices[adj] = index + 1;
			adj++;
		}

		// Left
		if(w - 1 > 0)
		{
			if(h > 0)
			{
				indices[adj] = index - width - 1;
				adj++;
			}
			if(h < height - 1)
			{
				indices[adj] = index - width + 1;
				adj++;
			}
		}
		// Right

		if(w + 1 < width - 1)
		{
			if(h > 0)
			{
				indices[adj] = index + width - 1;
				adj++;
			}
			if(h < height - 1)
			{
				indices[adj] = index + width + 1;
				adj++;
			}
		}


		adjacent = new int[adj]; // pare down array to existing neighbor count
		for(int i = 0; i < adj; i++)
			adjacent[i] = indices[i]; 
	}

	public virtual void CheckHorizontal(ref Cell3D[] cells, bool curPhase){}
	public virtual bool CheckVertical(ref Cell3D[] cells, bool curPhase){return false;}
	public virtual void GetVerticals(int cellCount){}

	#endregion
}
