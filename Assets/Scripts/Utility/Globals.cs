/*	Globals should be kept to a minimum */
/*  Using Globals in order to assist with type ahead and to prevent mispelling code */
																				   
public enum BUTTONS
{
	REVEALSETTINGS,
	HIDESETTINGS,
	EDITCELLS,
	RESUMEGAME,
	RESETGAME,
	PLAY,
	PAUSE,
	NEXT,
	THREEDIM,
	TWODIM
}
