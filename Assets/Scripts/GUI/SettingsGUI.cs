﻿using UnityEngine;
using System.Collections;

/*
 * SettingsGUI.cs is a simple and dirty menu to allow settings to be edited, game to be restarted or paused, or the petri dish to be manipulated
 */
public class SettingsGUI : MonoBehaviour 
{
	#region vars
	public float hideSpeed; // Speed to hide/show the GUI pane
	public GUISkin skin;
	public GUIControl settings; // Rectangle objects, mostly. Easier to edit from the inspector
	public AnimationCurve curve; // Using an animation curve to get a more even and controllabe feel than just straight Lerp

	[HideInInspector]
	public GameManager manager;
	[HideInInspector]
	public PetriDish petriDishSettings;

	protected bool paused;

	#endregion

	#region Unity Methods
	void Start ()
	{
		settings.infoBox = settings.mainBox;
		settings.infoBox.x = settings.visibleX;
		
		settings.mainBox.x = settings.hiddenX; // Hide GUI
	}
	
	void Update () 
	{

	}

	void OnGUI()
	{

		settings.SetY(Screen.height - settings.bgBox.height);
		settings.infoBox.y = settings.camButton.y - settings.camButton.height;
		if(!settings.isLifeAltering) // If we're not editing the petri dish, allow the option to edit
		{
			if(GUI.Button (settings.activateButton, (settings.isVisible ? "Hide " : "Edit ") + "Settings"))
			{
				manager.Button(settings.isVisible ? BUTTONS.HIDESETTINGS : BUTTONS.REVEALSETTINGS);
			}
		}

		if(!settings.isVisible)
		{
			if (!manager.stateInfo.isThreeDim)
			{
				if(GUI.Button (settings.camButton, (settings.isLifeAltering ? "Resume Simulation" : "Edit Nodes")))
				{
					manager.Button(settings.isLifeAltering ? BUTTONS.RESUMEGAME : (settings.isVisible ? BUTTONS.HIDESETTINGS : BUTTONS.EDITCELLS));
				}
			}

			Rect play = settings.camButton;
			play.x += settings.camButton.width + 10;

			if(!settings.isLifeAltering)
			{
				if(GUI.Button(play, paused ? "Play" : "Pause"))
				{
					manager.Button(paused ? BUTTONS.PLAY : BUTTONS.PAUSE);
					paused = !paused;
				}

				if(paused)
				{
					play.x += settings.camButton.width + 10;
					if(GUI.Button(play, "Step"))
						manager.Button(BUTTONS.NEXT);
				}
				play.x += settings.camButton.width + 10;
				if(GUI.Button(play, "Reset Game"))
				{
					paused = false;
					manager.Button(BUTTONS.RESETGAME);
				}
			}
		}

		//Basic info display for petri dish settings
		settings.stepsBox.x = Screen.width - settings.stepsBox.width - 10;

		GUI.Label(settings.stepsBox, "Steps: " + manager.stateInfo.steps.ToString() + "  Current Live Cells: " + manager.stateInfo.liveCells);
		

		GUI.Label(settings.infoBox, "Scroll Wheel = zoom | Left Shift + Mouse = Rotate");

		if(!settings.isVisible)
			return;

		GUI.skin = skin;
		GUILayout.BeginArea(settings.mainBox, "");
		GUI.Box(settings.bgBox, "");
		float y = 0;
		float ry = 30;
		GUILayout.BeginHorizontal();
		manager.settings.gameSpeed = (int)GUI.HorizontalSlider(new Rect(5, ry, 100, 20), manager.settings.gameSpeed, 1, 20);
		GUI.Label(new Rect(90, y, 120, 60), "Game Speed: " + manager.settings.gameSpeed);	
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		manager.settings.gridWidth = (int)GUI.HorizontalSlider(new Rect(5, ry+=20, 100, 20), manager.settings.gridWidth, 4, 20);
		GUI.Label(new Rect(90, y+=20, 120, 60), "Grid Size: " + manager.settings.gridWidth);	
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		manager.settings.initialPercent = (int)GUI.HorizontalSlider(new Rect(5, ry+=20, 100, 20), manager.settings.initialPercent, 10, 100);
		GUI.Label(new Rect(90, y+=20, 120, 60), "Initial Cell %: " + manager.settings.initialPercent);	
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		manager.cam.speed = GUI.HorizontalSlider(new Rect(5, ry+=20, 100, 20), (Mathf.Round(manager.cam.speed*100)*.01f), 0f, .5f);
		GUI.Label(new Rect(90, y+=20, 120, 80), "Cam Rotation: " + manager.cam.speed);	
		GUILayout.EndHorizontal();

		
		GUILayout.BeginHorizontal();
		if (GUI.Button(new Rect(5, ry += 20, 100, 20), "Switch to " + (manager.stateInfo.isThreeDim ? "2D" : "3D")))
		{
			manager.stateInfo.reset = true;
			manager.stateInfo.isThreeDim = !manager.stateInfo.isThreeDim;
			manager.Button(BUTTONS.HIDESETTINGS);
		}
		GUILayout.EndHorizontal();
		

		GUILayout.EndArea();
	}
	#endregion

	#region Actions
	/// <summary>
	/// Move the GUI pane in or out of the view
	/// </summary>
	/// <param name="t"></param>
	public void Slide(float t)
	{
		Rect temp = settings.mainBox;
		temp.x = Mathf.Lerp(settings.hiddenX, settings.visibleX, t);
		settings.mainBox = temp;
	}
	#endregion
}

#region Helper Class

/*
 * A helper class for conveniece, so that variables may be accessed and set in the Inspector, and also "hidden" when not needed
 */
[System.Serializable]
public class GUIControl
{
	[HideInInspector]
	public bool isVisible;
	[HideInInspector]
	public bool isLifeAltering;

	public float hiddenX;
	public float visibleX;

	public Rect activateButton;
	public Rect camButton;

	public Rect bgBox;
	public Rect mainBox;
	public Rect stepsBox;
	public Rect infoBox;

	public Rect HorizontalOffset(Rect rect, int off)
	{
		Rect r = rect;
		r.x += off;
		return r;
	}

	public Rect VerticalOffset(Rect rect, int off)
	{
		Rect r = rect;
		r.y = off;
		return r;
	}

	public void SetY(float y)
	{
		mainBox.y = y - 100;
		camButton.y = activateButton.y = y + bgBox.height - 50;
	}


}
#endregion

















